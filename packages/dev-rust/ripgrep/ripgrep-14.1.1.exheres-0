# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo  [ rust_minimum_version=1.72 ]
require github [ user=BurntSushi ]
require bash-completion
require zsh-completion

SUMMARY="Line oriented search tool using Rust's regex library."
DESCRIPTION="
Line oriented search tool using Rust's regex library. Combines the raw performance of grep with the
usability of the silver searcher.
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"

MYOPTIONS="
    pcre2 [[ description = [ Support Perl Compatible Regular Expressions via libpcre2 ] ]]
"

DEPENDENCIES="
    build+run:
        pcre2? ( dev-libs/pcre2 )
"

ECARGO_FEATURE_ENABLES=( 'pcre2 pcre2' )
CARGO_SRC_TEST_PARAMS=( --all )

BASH_COMPLETIONS=( rg.bash )
ZSH_COMPLETIONS=( _rg )

src_install() {
    cargo_src_install

    if option bash-completion ; then
        target/$(rust_target_arch_name)/release/rg --generate complete-bash > rg.bash
        bash-completion_src_install
    fi

    if option zsh-completion ; then
        target/$(rust_target_arch_name)/release/rg --generate complete-zsh > _rg
        zsh-completion_src_install
    fi

    target/$(rust_target_arch_name)/release/rg --generate man > rg.1
    doman rg.1
}

